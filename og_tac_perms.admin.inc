<?php
/**
 * @file
 * og_tac_perms.admin.inc
 *
 * Declares admin form config page
 * Handles tac entity references field creation based on user config changes
 */

/**
 * Admin configuration form function
 *
 */
function og_tac_perms_admin_settings() {
  $group_options = array();
  $group_content_options = array();

  $groups = og_get_all_group_bundle();

  if (isset($groups['node'])) {
    foreach ($groups['node'] as $key => $value) {
      $group_options[$key] = $value;
    }
  }

  $content_groups = og_get_all_group_content_bundle();

  if (isset($content_groups['node'])) {
    foreach ($content_groups['node'] as $key => $value) {
      $group_content_options[$key] = $value;
    }
  }

  $form['og_tac_group_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enable OG TAC permission options for Group nodes'),
    '#options' => $group_options,
    '#default_value' => variable_get('og_tac_group_node_types', array()),
    '#description' => t('Check the box next to the group node types to enable TAC for that group'),
  );

  $form['og_tac_group_content_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enable OG TAC for Group content nodes'),
    '#options' => $group_content_options,
    '#default_value' => variable_get('og_tac_group_content_node_types', array()),
    '#description' => t('Check the box next to the group content node type to utitilze group TAC permissioning for view access for that group content'),
  );

  $form['#submit'][] = 'og_tac_perms_admin_settings_submit';

  return system_settings_form($form, TRUE);
}

/**
 * Admin configuration page submit handler
 *
 */
function og_tac_perms_admin_settings_submit($form, &$form_state) {
  variable_set('og_tac_group_node_types', $form_state['values']['og_tac_group_node_types']);
  variable_set('og_tac_group_content_node_types', $form_state['values']['og_tac_group_content_node_types']);

  foreach ($form_state['values']['og_tac_group_node_types'] as $key => $value) {
    if ($value) {
      if (!field_info_instance('node', 'og_tac_vocabs', $value)) {

        og_tac_perms_create_og_tac_controller_field();

        $instance = array(
          'field_name' => 'og_tac_vocabs',
          'entity_type' => 'node',
          'bundle' => $value,
          'label' => 'OG TAC Controller Vocabularies',
          'description' => 'Select which vocabularies will be used to control TAC for group content of this group.',
          'widget' => array(
            'type' => 'options_select',
          ),
        );
        $new_instance = field_create_instance($instance);
      }//end if no instance exists....
    }//end if value is not = 0
  }//end foreach group node types

  foreach ($form_state['values']['og_tac_group_content_node_types'] as $key => $value) {
    if ($value) {
      if (!field_info_instance('node', 'og_tac_terms', $value)) {

        og_tac_perms_create_og_tac_terms_field();

        $instance = array(
          'field_name' => 'og_tac_terms',
          'entity_type' => 'node',
          'bundle' => $value,
          'label' => 'OG TAC Content Terms',
          'description' => 'Select which terms will be used to control visibility',
          'widget' => array(
            'type' => 'options_select',
          ),
        );
        $new_instance = field_create_instance($instance);
      }//end if no instance exists....
    }//end if value is not = 0
  }//end foreach group content types
}
